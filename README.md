# hru-delin

Project for Michael Rabotin
Goal is to make a container ready for hru-delin on MEso@LR

## How to build
```bash
singularity build hru-delin.sif hru-delin.def
```

## How to run
First pull the image then run whatever =>
```bash
singularity pull oras://registry.forgemia.inra.fr/singularity-mesolr/hru-delin/hru-delin:latest
./hru-delin_latest.sif "/opt/hru-delin/bin/hru-delin_step1.sh -c -p 1 -f /root/tiller.cfg"
singularity exec hru-delin.sif bash -c "cd /opt/hru-delin && export GISBASE=/usr/lib/grass78 && export PYTHONPATH=/usr/lib/grass78/etc/python/ && export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/grass78/lib && export GIS_LOCK=$$ && exec /opt/hru-delin/bin/hru-delin_step1.sh -c -p 1 -f /root/tiller.cfg"


```

## TODO
- %test part of def file

